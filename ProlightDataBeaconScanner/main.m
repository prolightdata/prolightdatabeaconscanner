//
//  main.m
//  ProlightDataBeaconScanner
//
//  Created by BeaconTestMachin on 08/09/2015.
//  Copyright (c) 2015 prolight_data. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BeaconScanner.h"



void runTest(char nameOfTest[]) {
    
    NSString *testName = [NSString stringWithUTF8String:nameOfTest];
    NSLog(@"Name of test: %@", testName);
    NSUserDefaults *ud=[NSUserDefaults standardUserDefaults];
    
    NSArray *arguments = [[NSProcessInfo processInfo] arguments];
    
    if ([arguments containsObject:@"-help"] || [arguments containsObject:@"-h"]) {
        exit(0);
    }
    
    BeaconScanner *scanner = [[BeaconScanner alloc] initWithTestName:testName];
    [scanner startScanningWithBlock:^(NSDictionary *dict) {
        
        if ([ud objectForKey:@"outputType"]) {
            if ([[ud objectForKey:@"outputType"] isEqualToString:@"json"]) {
                NSError *err;
                NSData * jsonData = [NSJSONSerialization dataWithJSONObject:dict options:0 error:&err];
                NSString * myString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
                printf("%s\n",[myString UTF8String]);
            }
        }
        else {
            [dict enumerateKeysAndObjectsUsingBlock:^(NSString * key, NSString *obj, BOOL *stop) {
                printf("%s:%s ",[key UTF8String],[obj UTF8String]);
            }];
            printf("\n");
        }
        
    } errorBlock:^(NSString *error) {
        NSLog(@"%@",error);
        exit(-1);
    }];
    
    int scantime=0;
    NSDate *now=[NSDate date];
    if ([ud objectForKey:@"ScanTime"]) scantime=[[ud objectForKey:@"ScanTime"] intValue];
    
    while (true) {
        if (scantime>0) {
            sleep(1);
            if (fabs([now timeIntervalSinceNow])>scantime) {
                exit(0);
                
            }
        }
        else {
            sleep(1);
        }
    }
}

int main(int argc,   char * argv[]) {
    @autoreleasepool {
        char str[50] = {0};
        printf("Enter test name: ");
        scanf("%s", str);
        runTest(str);
        
        return 0;
    }
}




