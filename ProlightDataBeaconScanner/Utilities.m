//
//  Utilities.m
//  ProlightDataBeaconScanner
//
//  Created by BeaconTestMachin on 08/09/2015.
//  Copyright (c) 2015 prolight_data. All rights reserved.
//

#import "Utilities.h"
#import "AFNetworking.h"

@implementation Utilities

- (NSString *)machineName {
    
    NSHost *host;
    host = [NSHost currentHost];
    NSString *name = [host name];
    return name;
}

- (NSString *)testTime {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    NSDate *time = [NSDate date];
    
    [formatter setDateFormat:@"yyyy-mm-dd hh:mm:ss"];
    [formatter setTimeZone: [NSTimeZone timeZoneWithName:@"UTC"]];
    NSString *dateString = [formatter stringFromDate:time];
    
    return dateString;
}

- (NSString *)testUUID {
    NSUUID *testId = [NSUUID UUID];
    NSString *uuidString = [testId UUIDString];
    return uuidString;
}

- (void)postTestDataToServer:(NSDictionary *)dict {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager POST:@"http://prolightdata.toughcrab.com:8001/api/beacon-data"
       parameters:dict
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              NSLog(@"JSON: %@", responseObject);
          } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              NSLog(@"Error: %@", error);
          }];
}

- (void)writeTestDataToFile:(NSDictionary *)dict {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"test_data.txt"];
    
    NSError *writeError = nil;
    NSData* data;
    
    data = [NSJSONSerialization dataWithJSONObject:dict options:NSJSONWritingPrettyPrinted error:&writeError];
    
    if (filePath) {
        [self writeToLogFile:data];
    }

}

- (void)writeToLogFile:(NSData *)data{

    NSString *documentsDirectory = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
    NSString *fileName = [documentsDirectory stringByAppendingPathComponent:@"test_data.txt"];
    
    NSFileHandle *fileHandle = [NSFileHandle fileHandleForWritingAtPath:fileName];
    if (fileHandle){
        [fileHandle seekToEndOfFile];
        [fileHandle writeData:data];
        [fileHandle closeFile];
    }
    else{
        [data writeToFile:fileName atomically:YES];
    }
}

@end
