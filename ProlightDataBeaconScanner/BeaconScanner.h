//
//  BeaconScanner.h
//  ProlightDataBeaconScanner
//
//  Created by BeaconTestMachin on 08/09/2015.
//  Copyright (c) 2015 prolight_data. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreBluetooth/CoreBluetooth.h>
#import "Utilities.h"//

@interface BeaconScanner : NSObject <CBCentralManagerDelegate>

- (id)initWithTestName:(NSString *)testName;

-(void)startScanningWithBlock:(void (^)(NSDictionary *))inScanningBlock errorBlock:(void (^)(NSString *error))inErrorBlock;
-(void)stopScanning;
-(void)resumeScanning;

@property (nonatomic, strong) NSString *nameOfMachine;
@property (nonatomic, strong) NSString *timeOfTest;
@property (nonatomic, strong) NSString *testId;
@property (nonatomic, strong) NSString *testName;
@property (nonatomic, strong) NSDictionary *testData;

@end
