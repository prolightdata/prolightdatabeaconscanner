//
//  Utilities.h
//  ProlightDataBeaconScanner
//
//  Created by BeaconTestMachin on 08/09/2015.
//  Copyright (c) 2015 prolight_data. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Utilities : NSObject

- (NSString *)machineName;
- (NSString *)testTime;
- (NSString *)testUUID;
- (void)postTestDataToServer:(NSDictionary *)dict;
- (void)writeTestDataToFile:(NSDictionary *)dict;

@end
